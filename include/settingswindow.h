﻿#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QWidget>
#include <memory>

#include "player.h"

/* TODO
 * Add filtering groups via regex.
 */
namespace Ui {
class SettingsWindow;
}

/*!
 * \brief Window with settings for the application.
 */
class SettingsWindow : public QWidget {
  Q_OBJECT

public:
  /*!
   * \brief Constructs settings window.
   * \param player pointer to player which stores groups/tracks.
   * \param parent parent class.
   */
  explicit SettingsWindow(Player *player, QWidget *parent = nullptr);
  ~SettingsWindow();

private slots:
  /*!
   * \brief Filters groups displayed in combo box.
   * \param filter filter applied to group names.
   */
  void filterGroups(QString filter);
  /*!
   * \brief Opens file dialog and adds chosen files as tracks to current group.
   */
  void addTracks();
  /*!
   * \brief Opens dialog and adds a new group with given parameters.
   */
  void addGroup();
  /*!
   * \brief Removes group from combo box.
   * \param groupName name of the deleted group.
   */
  void removeGroup(QString groupName);
  /*!
   * \brief Changes displayed name of the group in combo box.
   * \param oldName old name of the group.
   * \param newName new name of the group.
   */
  void renameGroup(QString oldName, QString newName);

private:
  /*!
   * \brief Adds tracks to current group skipping incorrect ones.
   * \param filenames list of filenames of files that are to be made tracks.
   */
  void addFiles(const QStringList &filenames);
  /*!
   * \brief Handles file dropping in section 'Add tracks'.
   * \param watched pointer to watched object (ui->addTracks).
   * \param event pointer to event.
   * \return true if the event has been handled, false otherwise.
   */
  bool eventFilter(QObject *watched, QEvent *event) override;
  /*!
   * \brief Sets key sequences of hotkeys in ui and connects changes to player.
   */
  void setUpHotkeys();

  //! Pointer to ui form generated from settingswindow.ui.
  Ui::SettingsWindow *ui;
  //! Pointer to player which stores groups/tracks.
  Player *player;
  //! Pointer to currently selected group.
  std::weak_ptr<Group> currentGroup;
};

#endif // SETTINGSWINDOW_H
