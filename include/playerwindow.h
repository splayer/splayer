﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>

#include "player.h"
#include "settingswindow.h"

namespace Ui {
class PlayerWindow;
}

/*!
 * \brief Main window of the application.
 * Main window with GUI for control over currently played track.
 */
class PlayerWindow : public QWidget {
  Q_OBJECT

public:
  /*!
   * \brief Constructs player window.
   * \param parent parent widget.
   */
  explicit PlayerWindow(QWidget *parent = nullptr);
  ~PlayerWindow();

private slots:
  /*!
   * \brief Launches settings window, or if it exists raises it.
   */
  void launchSettings();
  /*!
   * \brief Updates playback button to display play or pause button.
   */
  void playback();
  /*!
   * \brief Updates GUI to display information about new track.
   * \param title title of the track.
   * \param author author of the track.
   * \param length length of the track in milliseconds.
   * \param globalVolume global volume (should be between 0 and 100).
   * \param groupVolume group volume (should be between 0 and 100).
   * \param trackVolume track volume (should be between 0 and 100).
   * \param groupWeight weight of the track's group.
   * \param trackWeight weight of the track.
   */
  void newTrack(QString title, QString author, qint64 length,
                unsigned globalVolume, const unsigned groupVolume,
                unsigned trackVolume, unsigned groupWeight,
                unsigned trackWeight);
  /*!
   * \brief Updates progress bar to display given position.
   * \param position position of progress bar (should be less than current max).
   */
  void progressChanged(qint64 position);
  /*!
   * \brief Takes appropriate action to respond to activation of hotkey.
   * \param hotkey activated hotkey.
   */
  void manageHotkey(Player::Hotkey hotkey);

private:
  //! Pointer to ui form generated from playerwindow.ui.
  Ui::PlayerWindow *ui;
  //! Pointer to settings window or nullptr if settings window does not exist.
  SettingsWindow *settingsWindow;
  //! Pointer to player responsible for managing groups/tracks.
  Player player;
  //! Determines if a track is being played now.
  bool playing;
};

#endif // MAINWINDOW_H
