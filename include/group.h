﻿#ifndef GROUP_H
#define GROUP_H

#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QWidget>
#include <list>
#include <memory>
#include <unordered_map>

#include "track.h"

/* TODO
 * Make a nice UI for Group
 * (I imagine search box on top, similar to the one in SettingsWindow, for
 * filtering tracks, below or next to it volume/weight controls and then
 * tracks in vertical layout in order from the list "tracks"; maybe use
 * some sort of scroll area?; probably some buttons next to each track to
 * remove them?; it would be nice if user could drag tracks to rearrange them,
 * but i doubt we'll have time for that).
 * Maybe make it possible to change group's name?
 * Connect ui to some slots (maybe make setVolume/Weight slots?).
 */
namespace Ui {
class Group;
}

class Group : public QWidget {
  Q_OBJECT

public:
  /*!
   * \brief Constructs a group based on data from groupObject.
   * \param groupObject should contain "name", "weight", "volume" and "tracks".
   * \throws std::invalid_argument if argument misses valid "name" or "tracks".
   */
  explicit Group(const QJsonObject &groupObject);
  /*!
   * \brief Constructs empty group with given parameters.
   * \param name name of the group.
   * \param weight weight of the group.
   * \param volume volume of the group.
   */
  explicit Group(QString name, unsigned volume = 50, unsigned weight = 100);
  ~Group();

  /*!
   * \brief Gets name of the group.
   * \return name of the group.
   */
  QString getName() const;
  /*!
   * \brief Gets volume of the group.
   * \return volume of the group.
   */
  unsigned getVolume() const;
  /*!
   * \brief Gets weight of the group.
   * \return weight of the group.
   */
  unsigned getWeight() const;
  /*!
   * \brief Gets total weight of tracks of the group.
   * \return sum of weight of tracks of the group multiplied by group's weight.
   */
  double getTotalWeight() const;
  /*!
   * \brief Adds given track to the group.
   * \param track track to be added to the group.
   */
  void addTrack(std::shared_ptr<Track> track);
  /*!
   * \brief Contains if in the group is already a track with the same url
   * \param track to be checked
   * \return true if suck a track is already in the group else false
   */
  bool contains(std::shared_ptr<Track> track);
  /*!
   * \brief Sets volume of the group.
   * \param volume new volume of the group.
   */
  void setVolume(unsigned volume);
  /*!
   * \brief Sets weight of the group.
   * \param weight new weight of the group.
   */
  void setWeight(unsigned weight);
  /*!
   * \brief Gets a track with given path.
   * \param path path of a track.
   * \throws std::out_of_range if there is track with such path in the group.
   * \return std::shared_ptr to the track with given path.
   */
  std::shared_ptr<Track> getTrack(QString path) const;
  /*!
   * \brief Chooses appropriate track based on given weight.
   * \param weight weight (should be between 0 and getTotalWeight).
   * \throws std::out_of_range is an appropriate track cannot be found.
   * \return std::shared_ptr to appropriate track.
   */
  std::shared_ptr<Track> chooseTrack(double weight) const;
  /*!
   * \brief Constructs a QJsonObject based on the group's parameters.
   * \return QJsonValue with data about the group.
   */
  QJsonValue toJson() const;

signals:
  /*!
   * \brief Signals that the groups should be removed.
   * \param name name of the group.
   */
  void remove(QString name);
  /*!
   * \brief Signals that the name of the group has changed.
   * \param oldName old name of the group.
   * \param oldName new name of the group.
   */
  void nameChanged(QString oldName, QString newName);

private slots:
  /*!
   * \brief Creates dialog allowing user to change name of the group.
   */
  void changeName();
  /*!
   * \brief Removes selected tracks.
   */
  void removeSelected();
  /*!
   * \brief Filters visible tracks.
   * \param filter filter applied to track filenames.
   */
  void filterTracks(QString filter);

private:
  //! Pointer to ui form generated from group.ui.
  Ui::Group *ui;
  //! Name of the group.
  QString name;
  //! Volume of the group.
  unsigned volume;
  //! Weight of the group.
  unsigned weight;
  //! Sum of weights of all tracks of the group.
  double tracksWeight;
  //! List of tracks contained in the group.
  std::list<std::shared_ptr<Track>> tracks;
  //! Map of iterators to list of tracks. First item is path of the track.
  std::unordered_map<QString, std::list<std::shared_ptr<Track>>::iterator>
      tracksMap;
};

#endif // GROUP_H
