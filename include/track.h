﻿#ifndef TRACK_H
#define TRACK_H

#include <QJsonObject>
#include <QJsonValue>
#include <QMediaObject>
#include <QMediaPlayer>
#include <QString>
#include <QUrl>
#include <QWidget>

/* TODO
 * Make a nice UI for Track
 * (Perhaps add static QMediaObject to Track to be able to display title?;
 * remember they will be displayed in a list vertically, so it would be nice
 * if all the info/controls were on one line).
 * Connect ui to some slots (maybe make setVolume/Weight slots?).
 */
namespace Ui {
class Track;
}

class Track : public QWidget {
  Q_OBJECT

public:
  /*!
   * \brief Constructs a track based on data from trackObject.
   * \param group name of the group to which the track belongs.
   * \param trackObject should contain "path", "volume" and "weight".
   * \throws std::invalid_argument if trackObject misses valid "path".
   */
  explicit Track(QString group, const QJsonObject &trackObject);
  /*!
   * \brief Constructs a track with given parameters.
   * \param group name of the group to which the track belongs.
   * \param path path to a file with music.
   * \param volume volume of the track.
   * \param weight weight of the track.
   */
  explicit Track(QString group, QUrl path, unsigned volume = 50,
                 unsigned weight = 100);
  /*!
   * \brief Emits endOfTrack.
   */
  ~Track();

  /*!
   * \brief Gets name of the group to which the track belongs.
   * \return name of the group to which the track belongs.
   */
  QString getGroup() const;
  /*!
   * \brief Gets path of the track.
   * \return path of the track.
   */
  QUrl getPath() const;
  /*!
   * \brief Gets volume of the track.
   * \return volume of the track.
   */
  unsigned getVolume() const;
  /*!
   * \brief Gets weight of the track.
   * \return weight of the track.
   */
  unsigned getWeight() const;
  /*!
   * \brief Constructs a QJsonObject based on the track's parameters.
   * \return QJsonValue with data about the track.
   */
  QJsonValue toJson() const;
  /*!
   * \brief Plays the track on given media player.
   * \param player Media player on which the track is to be played.
   * \param lastTrack Determines if the app should quit after track ends.
   */
  void play(QMediaPlayer *player, const bool *lastTrack);
  /*!
   * \brief Determines if the track is selected.
   * \return true if the track is selected, false otherwise.
   */
  bool isSelected();

public slots:
  /*!
   * \brief Sets volume of the track.
   * \param volume new volume of the track.
   */
  void setVolume(unsigned volume);
  /*!
   * \brief Sets weight of the track.
   * \param weight new weight of the track.
   */
  void setWeight(unsigned weight);
  /*!
   * \brief Handles request to play previous track.
   */
  void previous();
  /*!
   * \brief Handles request to play next track.
   */
  void next();

signals:
  /*!
   * \brief Signals that a new track has been loaded by media player.
   */
  void loaded();
  /*!
   * \brief Signals that the track has ended and a new one may be played.
   * \param forward determines if the new track should be previous or next.
   */
  void endOfTrack(bool forward = true);
  /*!
   * \brief Signals that the track's weight has changed.
   * \param oldWeight weight of the track before change.
   * \param newWeight weight of the track after change.
   */
  void changedWeight(unsigned oldWeight, unsigned newWeight);

private:
  //! Pointer to ui form generated from track.ui.
  Ui::Track *ui;
  //! Name of the group to which the track belongs.
  QString group;
  //! Path to a file with music.
  const QUrl path;
  //! Volume of the track.
  unsigned volume;
  //! Weight of the track.
  unsigned weight;
  //! Pointer to media player on which the track is playing or nullptr.
  QMediaPlayer *player;
  //! Determines if a we should close the app after track ends.
  const bool *lastTrack;
};

#endif // TRACK_H
