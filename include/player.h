﻿#ifndef PLAYER_H
#define PLAYER_H

#include <QHotkey>
#include <QMediaPlayer>
#include <QObject>
#include <QString>
#include <list>
#include <map>
#include <memory>
#include <vector>

#include "group.h"
#include "track.h"

//! Used to grant SettingsWindow access to some Player's private members.
class PlayerAttorney;

class Player : public QObject {
  Q_OBJECT

public:
  /*!
   * \brief Loads data from save file and sets up the player.
   */
  Player();
  /*!
   * \brief Saves data to save file.
   */
  ~Player();

  enum class Hotkey {
    previous,
    next,
    playback,
    stop,
    settings,
    query,
    trackVolUp,
    groupVolUp,
    globalVolUp,
    trackVolDown,
    groupVolDown,
    globalVolDown
  };
  Q_ENUM(Hotkey);
  static QMetaEnum metaHotkey;

public slots:
  /*!
   * \brief Plays or pauses current track.
   */
  void playback();
  /*!
   * \brief Marks or ummarks current track as the last one.
   */
  void stop();

  /*!
   * \brief Sets progress of currently played track.
   * \param value new position (should be between 0 and length of track).
   */
  void setProgress(qint64 value);

  /*!
   * \brief Sets global volume.
   * \param volume new global volume.
   */
  void setGlobalVolume(unsigned volume);
  /*!
   * \brief Sets current group's volume.
   * \param volume new group's volume.
   */
  void setGroupVolume(unsigned volume);
  /*!
   * \brief Sets current track's volume.
   * \param volume new track's volume.
   */
  void setTrackVolume(unsigned volume);

  /*!
   * \brief Sets current group's weight.
   * \param weight new group's weight.
   */
  void setGroupWeight(unsigned weight);
  /*!
   * \brief Sets current track's weight.
   * \param weight new track's weight.
   */
  void setTrackWeight(unsigned weight);
  /*!
   * \brief Sets requested hotkey to activate when given sequence is pressed.
   * \param hotkey hotkey which should be set.
   * \param sequence sequence of keys on which it should activate.
   */
  void setHotkey(Hotkey hotkey, const QKeySequence &sequence);
  /*!
   * \brief Gets key sequence associated with requested hotkey.
   * \param hotkey hotkey which sequence is requested.
   * \return sequence sequence of keys associated with requested hotkey.
   */
  QKeySequence getHotkey(Hotkey hotkey);
  /*!
   * \brief Adds group to the player.
   * \param group group to be added.
   */
  void addGroup(std::shared_ptr<Group> group);
signals:
  /*!
   * \brief Signals that previous track is supposed to be played.
   */
  void previous();
  /*!
   * \brief Signals that next track is supposed to be played.
   */
  void next();
  /*!
   * \brief Signals that a hotkey has been activated.
   * \param hotkey the hotkey that has been activated.
   */
  void hotkeyActivated(Hotkey hotkey);
  /*!
   * \brief Signals that a new track is being played (or paused).
   * \param title title of the track.
   * \param author author of the track.
   * \param length length of the track in milliseconds.
   * \param globalVolume global volume (should be between 0 and 100).
   * \param groupVolume group volume (should be between 0 and 100).
   * \param trackVolume track volume (should be between 0 and 100).
   * \param groupWeight weight of the track's group.
   * \param trackWeight weight of the track.
   */
  void newTrack(QString title, QString author, qint64 length,
                unsigned globalVolume, unsigned groupVolume,
                unsigned trackVolume, unsigned groupWeight,
                unsigned trackWeight);
  /*!
   * \brief Signals that progress of playing current track has changed.
   * \param position new position of current track.
   */
  void progressChanged(qint64 position);

private slots:
  /*!
   * \brief Plays previous track or does nothing if no such track exists.
   */
  void previousTrack();
  /*!
   * \brief Plays next track possibly choosing a new track to play.
   */
  void nextTrack();
  /*!
   * \brief Sets volume, possibly plays and emits new metadata using newTrack.
   */
  void updatePlayer();

private:
  /*!
   * \brief Attempts to load data from save file.
   */
  void loadFromFile();
  /*!
   * \brief Attempts to save data to save file.
   */
  void saveToFile();
  /*!
   * \brief Function calculating what volume should be used while playing.
   * \param globalVolume global volume.
   * \param groupVolume current track's volume.
   * \param trackVolume current track's group's volume.
   * \return calculated volume.
   */
  int volumeFunction(unsigned globalVolume, unsigned groupVolume,
                     unsigned trackVolume) const;
  /*!
   * \brief Makes current track play itself.
   */
  void playCurrent();
  /*!
   * \brief Chooses a new track to be played.
   * \return chosen track or empty weak_ptr.
   */
  std::weak_ptr<Track> chooseTrack() const;

  friend PlayerAttorney;
  //! Map of known groups containing tracks. First item is name of the group.
  std::map<QString, std::shared_ptr<Group>> groups;
  //! List to pointers of tracks that have been played.
  std::list<std::weak_ptr<Track>> played;
  //! Iterator to list item with currently played track or played.end().
  std::list<std::weak_ptr<Track>>::iterator current;
  //! Media player responsible for playing tracks and extracting metadata.
  QMediaPlayer player;
  //! Global volume of the player.
  int volume;
  //! Determines if a track is being played now.
  bool playing;
  //! Determines if a we should close the app after current track ends.
  bool lastTrack;
  //! Global hotkeys for convenient control over player.
  std::vector<std::shared_ptr<QHotkey>> hotkeys;
};

class SettingsWindow;

class PlayerAttorney {
  friend SettingsWindow;
  static std::map<QString, std::shared_ptr<Group>> &groups(Player *player) {
    return player->groups;
  }
  static const std::weak_ptr<Track> &currentTrack(Player *player) {
    return *player->current;
  }
  static void nextTrack(Player *player) { player->nextTrack(); }
};

#endif // PLAYER_H
