# SPlayer

## Requirements:
- Qt 5.10 or newer - [installation](https://www.qt.io/download)
- Qt Multimedia - ``sudo apt install libqt5multimedia5``
- qdep - [installation](https://github.com/Skycoder42/qdep#installation)

## Compilation

To compile the program run
```sh
qmake && make
```

You can then start the program by typing
```sh
./splayer
```

## TODO

This readme is incomplete.