﻿#include <QApplication>

#include "track.h"
#include "ui_track.h"

Track::Track(QString group, const QJsonObject &trackObject)
    : Track(group, trackObject["path"].toString(),
            trackObject["volume"].toInt(50), trackObject["weight"].toInt(100)) {
}

Track::Track(QString group, QUrl path, unsigned volume, unsigned weight)
    : ui(new Ui::Track), group(group), path(path), volume(volume),
      weight(weight), player(nullptr), lastTrack(nullptr) {
  ui->setupUi(this);
  if (path.toString() == "")
    throw std::invalid_argument("path is invalid");

  ui->checkBox->setText(path.fileName());

  // TODO update volume and weight if this track is currently played.
  ui->volume->setValue(volume);
  connect(ui->volume, &QAbstractSlider::valueChanged, this, &Track::setVolume);
  ui->weight->setValue(weight);
  connect(ui->weight, QOverload<int>::of(&QSpinBox::valueChanged), this,
          &Track::setWeight);
}

Track::~Track() { emit endOfTrack(); }

QString Track::getGroup() const { return group; }

QUrl Track::getPath() const { return path; }

unsigned Track::getVolume() const { return volume; }

unsigned Track::getWeight() const { return weight; }

void Track::setVolume(unsigned volume) { this->volume = volume; }

void Track::setWeight(unsigned weight) {
  emit changedWeight(this->weight, weight);
  this->weight = weight;
}

QJsonValue Track::toJson() const {
  QJsonObject trackJson;
  trackJson["path"] = path.toString();
  trackJson["volume"] = static_cast<int>(volume);
  trackJson["weight"] = static_cast<int>(weight);

  return trackJson;
}

void Track::play(QMediaPlayer *player, const bool *lastTrack) {
  this->player = player;
  this->lastTrack = lastTrack;
  connect(player, QOverload<QMediaPlayer::Error>::of(&QMediaPlayer::error),
          this, &Track::next); // If we couldn't load file, we skip the track.
  connect(player, &QMediaPlayer::mediaStatusChanged, this,
          [this](QMediaPlayer::MediaStatus status) {
            if (status == QMediaPlayer::MediaStatus::EndOfMedia)
              next();

            if (status == QMediaPlayer::MediaStatus::LoadedMedia ||
                status == QMediaPlayer::MediaStatus::BufferedMedia)
              emit loaded();
          });
  connect(player, &QMediaPlayer::durationChanged, this, &Track::loaded);
  player->setMedia(QUrl::fromLocalFile(getPath().toString()));
}

bool Track::isSelected() { return ui->checkBox->checkState(); }

void Track::previous() {
  if (*lastTrack) {
    qApp->quit();
    return;
  }
  disconnect(player, nullptr, this, nullptr);
  player = nullptr;
  lastTrack = nullptr;
  emit endOfTrack(false);
}

void Track::next() {
  if (*lastTrack) {
    qApp->quit();
    return;
  }
  disconnect(player, nullptr, this, nullptr);
  player = nullptr;
  lastTrack = nullptr;
  emit endOfTrack();
}
