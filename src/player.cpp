﻿#include <QApplication>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMediaMetaData>
#include <QRandomGenerator>
#include <QStandardPaths>

#include "player.h"

QMetaEnum Player::metaHotkey = QMetaEnum::fromType<Player::Hotkey>();

Player::Player()
    : current(played.end()), volume(50), playing(false), lastTrack(false),
      hotkeys(metaHotkey.keyCount()) {
  connect(&player, &QMediaPlayer::positionChanged, this,
          &Player::progressChanged);

  for (int i = 0; i < metaHotkey.keyCount(); ++i) {
    hotkeys[i] = std::make_shared<QHotkey>();
    connect(hotkeys[i].get(), &QHotkey::activated, [this, i]() {
      emit hotkeyActivated(static_cast<Hotkey>(metaHotkey.value(i)));
    });
  }
  loadFromFile();
}

Player::~Player() {
  saveToFile();

  // To prevent track from calling Player's slots after deletion of Player.
  std::shared_ptr<Track> track;
  if ((track = current->lock())) {
    disconnect(track.get(), nullptr, this, nullptr);
    disconnect(this, nullptr, track.get(), nullptr);
  }
}

void Player::playback() {
  if (playing)
    player.pause();
  else
    player.play();
  playing = !playing;
}

void Player::stop() { lastTrack = !lastTrack; }

void Player::setProgress(qint64 value) { player.setPosition(value); }

void Player::setGlobalVolume(unsigned volume) {
  if (played.empty()) // There is no current track.
    return;
  auto track = current->lock();
  auto group = groups.at(track->getGroup());
  this->volume = volume;
  player.setVolume(
      volumeFunction(volume, group->getVolume(), track->getVolume()));
}

void Player::setGroupVolume(unsigned volume) {
  if (played.empty()) // There is no current track.
    return;
  auto track = current->lock();
  auto group = groups.at(track->getGroup());
  group->setVolume(volume);
  player.setVolume(
      volumeFunction(volume, group->getVolume(), track->getVolume()));
}

void Player::setTrackVolume(unsigned volume) {
  if (played.empty()) // There is no current track.
    return;
  auto track = current->lock();
  auto group = groups.at(track->getGroup());
  track->setVolume(volume);
  player.setVolume(
      volumeFunction(volume, group->getVolume(), track->getVolume()));
}

void Player::setGroupWeight(unsigned weight) {
  if (played.empty()) // There is no current track.
    return;
  auto track = current->lock();
  auto group = groups.at(track->getGroup());
  group->setWeight(weight);
}

void Player::setTrackWeight(unsigned weight) {
  if (played.empty()) // There is no current track.
    return;
  auto track = current->lock();
  track->setWeight(weight);
}

void Player::setHotkey(Player::Hotkey hotkey, const QKeySequence &sequence) {
  hotkeys[static_cast<int>(hotkey)]->setShortcut(sequence);
  hotkeys[static_cast<int>(hotkey)]->setRegistered(!sequence.isEmpty());
}

QKeySequence Player::getHotkey(Player::Hotkey hotkey) {
  return hotkeys[static_cast<int>(hotkey)]->shortcut();
}

void Player::addGroup(std::shared_ptr<Group> group) {
  groups[group->getName()] = group;
  connect(group.get(), &Group::nameChanged, this, [this](QString oldName) {
    std::shared_ptr<Group> group = groups.at(oldName);
    groups[group->getName()] = group;
    groups.erase(oldName);
  });
  connect(group.get(), &Group::remove, this,
          [this](QString name) { groups.erase(name); });
}

void Player::previousTrack() {
  if (lastTrack) {
    qApp->quit();
    return;
  }
  if (current != played.begin()) {
    --current;

    if (current->expired()) { // The track may have been deleted.
      current = played.erase(current);
      previous();
      return;
    }
  }

  playCurrent();
}

void Player::nextTrack() {
  if (lastTrack) {
    qApp->quit();
    return;
  }
  // We are at the last track in played queue. It's time to choose a new track.
  if (current == played.end() || ++current == played.end()) {
    auto track = chooseTrack();

    if (!track.lock()) { // We couldn't get a track (probably there are none).
      emit newTrack("Open settings to add new tracks.",
                    "Until you do so there's not much else you can do here.", 0,
                    0, 0, 0, 0, 0);
      return;
    }
    played.push_back(track);
    current = std::prev(played.end());
  }

  if (current->expired()) { // The track may have been deleted.
    current = played.erase(current);
    nextTrack();
    return;
  }

  playCurrent();
}

void Player::updatePlayer() {
  std::shared_ptr<Track> track = current->lock();
  std::shared_ptr<Group> group = groups.at(track->getGroup());

  player.setVolume(
      volumeFunction(volume, group->getVolume(), track->getVolume()));
  if (playing)
    player.play();

  QString title = player.metaData(QMediaMetaData::Title).toString();
  if (title == "")
    title = QFileInfo(track->getPath().toString()).fileName();

  QString author = player.metaData(QMediaMetaData::Author).toString();
  if (author == "")
    author = player.metaData(QMediaMetaData::Composer).toString();
  if (author == "")
    author = player.metaData("composer").toString();
  if (author == "")
    author = player.metaData("ContributingArtist").toString();
  if (author == "")
    author = player.metaData(QMediaMetaData::AlbumArtist).toString();
  if (author == "")
    author = "author unknown";

  emit newTrack(title, author, player.duration(), volume, group->getVolume(),
                track->getVolume(), group->getWeight(), track->getWeight());
}

void Player::loadFromFile() {
  QFile file(QStandardPaths::writableLocation(QStandardPaths::HomeLocation) +
             "/.splayer.json");
  if (!file.open(QIODevice::ReadOnly))
    return;

  QJsonObject save = QJsonDocument::fromJson(file.readAll()).object();
  volume = save["volume"].toInt(50);

  // Load groups
  if (save.find("groups") != save.end()) {
    for (QJsonValue groupValue : save["groups"].toArray()) {
      try {
        addGroup(std::make_shared<Group>(groupValue.toObject()));
      } catch (std::invalid_argument &) {
        continue; // Invalid group, lets skip it.
      }
    }
  }

  // Load hotkeys.
  for (int i = 0; i < metaHotkey.keyCount(); ++i) {
    QKeySequence sequece(save[metaHotkey.key(i)].toString());
    setHotkey(static_cast<Hotkey>(metaHotkey.value(i)), sequece);
  }

  try { // Try to load last track played in previous instance of the app.
    played.push_back(groups.at(save["lastTrackGroup"].toString())
                         ->getTrack(save["lastTrackPath"].toString()));
  } catch (std::out_of_range &) {
    nextTrack(); // We cannot access last track so we choose a new one to play.
    return;
  }
  current = played.begin();
  playCurrent();
}

void Player::saveToFile() {
  QFile file(QStandardPaths::writableLocation(QStandardPaths::HomeLocation) +
             "/.splayer.json");
  if (!file.open(QIODevice::WriteOnly)) {
    qWarning().noquote() << "Unable to save data to \"" +
                                QStandardPaths::writableLocation(
                                    QStandardPaths::HomeLocation) +
                                "/.splayer.json\"";
    return;
  }

  QJsonObject save;
  save["volume"] = volume;
  if (current->lock()) {
    save["lastTrackGroup"] = current->lock()->getGroup();
    save["lastTrackPath"] = current->lock()->getPath().toString();
  }

  QJsonArray groupsJson;
  for (auto group : groups)
    groupsJson.append(group.second->toJson());
  save["groups"] = groupsJson;

  // Save hotkeys.
  for (int i = 0; i < metaHotkey.keyCount(); ++i) {
    save[metaHotkey.key(i)] =
        getHotkey(static_cast<Hotkey>(metaHotkey.value(i))).toString();
  }

  file.write(QJsonDocument(save).toJson());
}

int Player::volumeFunction(unsigned globalVolume, unsigned groupVolume,
                           unsigned trackVolume) const {
  return globalVolume * groupVolume * trackVolume / 10000;
}

void Player::playCurrent() {
  std::shared_ptr<Track> track = current->lock();

  disconnect(this, &Player::previous, nullptr, nullptr);
  disconnect(this, &Player::next, nullptr, nullptr);
  connect(this, &Player::previous, track.get(), &Track::previous);
  connect(this, &Player::next, track.get(), &Track::next);
  connect(track.get(), &Track::loaded, this, &Player::updatePlayer);
  connect(track.get(), &Track::endOfTrack, this, [this](bool forward) {
    std::shared_ptr<Track> track;
    if ((track = current->lock())) {
      disconnect(track.get(), nullptr, this, nullptr);
      disconnect(this, nullptr, track.get(), nullptr);
    }
    connect(this, &Player::previous, this, &Player::previousTrack);
    connect(this, &Player::next, this, &Player::nextTrack);

    if (forward)
      nextTrack();
    else
      previousTrack();
  });

  track->play(&player, &lastTrack);

  saveToFile();
}

std::weak_ptr<Track> Player::chooseTrack() const {
  double totalWeight = 0;
  for (auto group : groups) {
    totalWeight += group.second->getTotalWeight();
  }
  double random = QRandomGenerator::global()->bounded(totalWeight);

  std::weak_ptr<Track> track;
  for (auto group : groups) {
    if (group.second->getTotalWeight() >= random) {
      track = group.second->chooseTrack(random);
      break;
    }
    random -= group.second->getTotalWeight();
  }
  return track;
}
