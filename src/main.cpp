﻿#include <QApplication>
#include <QDir>
#include <QIcon>
#include <QLockFile>

#include "playerwindow.h"

int main(int argc, char *argv[]) {
  // Lock file to ensure only one instance of the application is running.
  QLockFile lockFile(QDir::temp().absoluteFilePath("splayer.lock"));
  if (!lockFile.tryLock(1000)) {
    return 0;
  }

  QApplication splayer(argc, argv);
  splayer.setWindowIcon(QIcon(":/resources/icon.png"));
  PlayerWindow mainWindow;
  mainWindow.show();

  int returnCode = splayer.exec();
  lockFile.unlock();
  return returnCode;
}
