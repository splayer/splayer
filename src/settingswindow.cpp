﻿#include <QAudioDeviceInfo>
#include <QDir>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QMimeData>
#include <QMimeDatabase>
#include <QRegularExpression>
#include <memory>

#include "settingswindow.h"
#include "ui_settingswindow.h"

// TODO refactor file

SettingsWindow::SettingsWindow(Player *player, QWidget *parent)
    : QWidget(parent, Qt::Window), ui(new Ui::SettingsWindow), player(player) {
  setAttribute(Qt::WA_DeleteOnClose);
  ui->setupUi(this);
  ui->manageTracks->setAcceptDrops(true);
  ui->manageTracks->installEventFilter(this);

  connect(ui->addTracks, &QPushButton::clicked, this,
          &SettingsWindow::addTracks);
  connect(ui->addGroup, &QPushButton::clicked, this, &SettingsWindow::addGroup);

  connect(ui->filterText, &QLineEdit::textChanged, this,
          &SettingsWindow::filterGroups);

  // Adding groups to combo box.
  for (auto group : PlayerAttorney::groups(player)) {
    ui->groups->addItem(group.second->getName());
    connect(group.second.get(), &Group::nameChanged, this,
            &SettingsWindow::renameGroup);
    connect(group.second.get(), &Group::remove, this,
            &SettingsWindow::removeGroup);
  }
  // Displaying current group;
  if (ui->groups->count() > 0) {
    currentGroup = PlayerAttorney::groups(player).at(ui->groups->currentText());
    ui->manageTracks->layout()->addWidget(currentGroup.lock().get());
    currentGroup.lock()->show();
  }
  // Handle selecting different group. TODO refactor.
  connect(
      ui->groups, QOverload<int>::of(&QComboBox::currentIndexChanged), this,
      [this]() {
        if (currentGroup.lock()) {
          currentGroup.lock()->setParent(nullptr);
          currentGroup.lock()->hide();
        }
        if (ui->groups->count() == 0)
          return;
        currentGroup =
            PlayerAttorney::groups(this->player).at(ui->groups->currentText());
        ui->manageTracks->layout()->addWidget(currentGroup.lock().get());
        currentGroup.lock()->show();
      });

  setUpHotkeys();
}

SettingsWindow::~SettingsWindow() {
  // Disinherit current group so that it doesn't get deleted.
  if (currentGroup.lock())
    currentGroup.lock()->setParent(nullptr);

  delete ui;
}

void SettingsWindow::filterGroups(QString filter) {
  QRegularExpression regex(filter, QRegularExpression::CaseInsensitiveOption);

  QString currentName = ui->groups->currentText();
  ui->groups->clear();
  for (auto group : PlayerAttorney::groups(player)) {
    QString groupName = group.second->getName();
    if (regex.match(groupName).hasMatch()) {
      ui->groups->addItem(groupName);
      if (ui->groups->itemText(ui->groups->count() - 1) == currentName)
        ui->groups->setCurrentIndex(ui->groups->count() - 1);
    }
  }
}

void SettingsWindow::addTracks() {
  addFiles(QFileDialog::getOpenFileNames(
      this, "Add tracks", QDir::homePath(),
      "Music (*.mp3 *.ogg *.wav *.pcm *.aiff "
      "*.acc *.wma *.flac *.alac *.wma);; All (*)"));
}

void SettingsWindow::addGroup() {
  QString name =
      QInputDialog::getText(this, tr("Add Group"), tr("Group name:"));
  if (name == "")
    return;
  int i; // Index at which we will add the new group.
  for (i = 0; i < ui->groups->count(); ++i) {
    if (ui->groups->itemText(i) == name) {
      QMessageBox::warning(this, tr("Add Group"),
                           "Failed to add; such group already exists");
      return;
    } else if (ui->groups->itemText(i) > name) {
      break;
    }
  }
  std::shared_ptr<Group> group = std::make_shared<Group>(name);
  PlayerAttorney::groups(player)[name] = group;
  connect(group.get(), &Group::remove, this, &SettingsWindow::removeGroup);
  connect(group.get(), &Group::nameChanged, this, &SettingsWindow::renameGroup);
  ui->groups->insertItem(i, group->getName());
  ui->groups->setCurrentIndex(i);
}

void SettingsWindow::removeGroup(QString groupName) {
  for (int i = 0; i < ui->groups->count(); ++i) {
    if (ui->groups->itemText(i) == groupName) {
      ui->groups->removeItem(i);
      break;
    }
  }
}

void SettingsWindow::renameGroup(QString oldName, QString newName) {
  for (int i = 0; i < ui->groups->count(); ++i) {
    if (ui->groups->itemText(i) == oldName) {
      ui->groups->setItemText(i, newName);
      break;
    }
  }
}

void SettingsWindow::addFiles(const QStringList &filenames) {
  static QStringList codecs =
      QAudioDeviceInfo::defaultOutputDevice().supportedCodecs();

  for (const QString &filename : filenames) {
    QFileInfo fileinfo(filename);
    if (!fileinfo.exists() || fileinfo.isDir()) {
      if (fileinfo.isDir())
        addFiles(QDir(filename).entryList());
      continue;
    }

    bool supported = false;
    QMimeType mime = QMimeDatabase().mimeTypeForFile(filename);
    if (QMediaPlayer::hasSupport(mime.name(), codecs) !=
        QMultimedia::SupportEstimate::NotSupported)
      supported = true;
    for (QString parentMime : mime.parentMimeTypes())
      if (QMediaPlayer::hasSupport(parentMime, codecs) !=
          QMultimedia::SupportEstimate::NotSupported)
        supported = true;

    if (supported) {
      std::shared_ptr<Track> track =
          std::make_shared<Track>(currentGroup.lock()->getName(), filename);
      currentGroup.lock()->addTrack(track);
      if (!PlayerAttorney::currentTrack(player).lock())
        PlayerAttorney::nextTrack(player);
    }
  }
}

bool SettingsWindow::eventFilter(QObject *watched, QEvent *event) {
  if (watched == ui->manageTracks && event->type() == QEvent::DragEnter) {
    QDragEnterEvent *dragEnterEvent = static_cast<QDragEnterEvent *>(event);

    if (dragEnterEvent->mimeData()->hasUrls())
      dragEnterEvent->acceptProposedAction();
    return true;
  } else if (watched == ui->addTracks && event->type() == QEvent::Drop) {
    QDropEvent *dropEvent = static_cast<QDropEvent *>(event);

    addFiles(QUrl::toStringList(dropEvent->mimeData()->urls()));
    return true;
  } else {
    return false;
  }
}

void SettingsWindow::setUpHotkeys() {
  // Set displayed sequences to those from player.
  ui->previousInput->setKeySequence(
      player->getHotkey(Player::Hotkey::previous));
  ui->nextInput->setKeySequence(player->getHotkey(Player::Hotkey::next));
  ui->playbackInput->setKeySequence(
      player->getHotkey(Player::Hotkey::playback));
  ui->stopInput->setKeySequence(player->getHotkey(Player::Hotkey::stop));
  ui->settingsInput->setKeySequence(
      player->getHotkey(Player::Hotkey::settings));
  ui->queryInput->setKeySequence(player->getHotkey(Player::Hotkey::query));
  ui->trackVolUpInput->setKeySequence(
      player->getHotkey(Player::Hotkey::trackVolUp));
  ui->groupVolUpInput->setKeySequence(
      player->getHotkey(Player::Hotkey::groupVolUp));
  ui->globalVolUpInput->setKeySequence(
      player->getHotkey(Player::Hotkey::globalVolUp));
  ui->trackVolDownInput->setKeySequence(
      player->getHotkey(Player::Hotkey::trackVolDown));
  ui->groupVolDownInput->setKeySequence(
      player->getHotkey(Player::Hotkey::groupVolDown));
  ui->globalVolDownInput->setKeySequence(
      player->getHotkey(Player::Hotkey::globalVolDown));

  // Connect changes to sequences to changing hotkeys in player.
  connect(ui->previousInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::previous, sequence);
          });
  connect(ui->nextInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::next, sequence);
          });
  connect(ui->playbackInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::playback, sequence);
          });
  connect(ui->stopInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::stop, sequence);
          });
  connect(ui->settingsInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::settings, sequence);
          });
  connect(ui->queryInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::query, sequence);
          });
  connect(ui->trackVolUpInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::trackVolUp, sequence);
          });
  connect(ui->groupVolUpInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::groupVolUp, sequence);
          });
  connect(ui->globalVolUpInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::globalVolUp, sequence);
          });
  connect(ui->trackVolDownInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::trackVolDown, sequence);
          });
  connect(ui->groupVolDownInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::groupVolDown, sequence);
          });
  connect(ui->globalVolDownInput, &QKeySequenceEdit::keySequenceChanged, this,
          [this](const QKeySequence &sequence) {
            player->setHotkey(Player::Hotkey::globalVolDown, sequence);
          });
}
