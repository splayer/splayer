﻿#include "playerwindow.h"
#include "ui_playerwindow.h"

PlayerWindow::PlayerWindow(QWidget *parent)
    : QWidget(parent), ui(new Ui::PlayerWindow), settingsWindow(nullptr),
      playing(false) {
  ui->setupUi(this);

  // Internal connections.
  connect(ui->settings, &QPushButton::clicked, this,
          &PlayerWindow::launchSettings);
  connect(ui->playback, &QToolButton::clicked, this, &PlayerWindow::playback);

  // Connect buttons to backend.
  connect(ui->previous, &QToolButton::clicked, &player, &Player::previous);
  connect(ui->next, &QToolButton::clicked, &player, &Player::next);
  connect(ui->playback, &QToolButton::clicked, &player, &Player::playback);
  connect(ui->stop, &QToolButton::clicked, &player, &Player::stop);

  // Connect volume & progress sliders to backend.
  connect(ui->globalVolumeSlider, &QAbstractSlider::valueChanged, &player,
          &Player::setGlobalVolume);
  connect(ui->groupVolumeSlider, &QAbstractSlider::valueChanged,
          [this](int value) { player.setGroupVolume(value); });
  connect(ui->trackVolumeSlider, &QAbstractSlider::valueChanged,
          [this](int value) { player.setTrackVolume(value); });
  connect(ui->progressBar, &QAbstractSlider::sliderMoved, &player,
          &Player::setProgress);

  // Connect weight spin-boxes to backend.
  connect(ui->groupWeight, QOverload<int>::of(&QSpinBox::valueChanged),
          [this](int value) { player.setGroupWeight(value); });
  connect(ui->trackWeight, QOverload<int>::of(&QSpinBox::valueChanged),
          [this](int value) { player.setTrackWeight(value); });

  // Connect backend changes to updating GUI.
  connect(&player, &Player::newTrack, this, &PlayerWindow::newTrack);
  connect(&player, &Player::progressChanged, this,
          &PlayerWindow::progressChanged);

  // Connect hotkeys.
  connect(&player, &Player::hotkeyActivated, this, &PlayerWindow::manageHotkey);
}

void PlayerWindow::launchSettings() {
  if (settingsWindow == nullptr) {
    settingsWindow = new SettingsWindow(&player, this);
    settingsWindow->show();
    connect(settingsWindow, &QObject::destroyed, this,
            [this]() { this->settingsWindow = nullptr; });
  } else {
    settingsWindow->raise();
  }
}

PlayerWindow::~PlayerWindow() { delete ui; }

void PlayerWindow::playback() {
  if (playing) {
    if (QIcon::hasThemeIcon("media-playback-start"))
      ui->playback->setIcon(QIcon::fromTheme("media-playback-start"));
    else
      ui->playback->setIcon(QIcon(":/resources/play.png"));
  } else {
    if (QIcon::hasThemeIcon("media-playback-start"))
      ui->playback->setIcon(QIcon::fromTheme("media-playback-pause"));
    else
      ui->playback->setIcon(QIcon(":/resources/pause.png"));
  }
  playing = !playing;
}

void PlayerWindow::newTrack(QString title, QString author, qint64 length,
                            unsigned globalVolume, unsigned groupVolume,
                            unsigned trackVolume, unsigned groupWeight,
                            unsigned trackWeight) {
  ui->title->setText(title);
  ui->author->setText(author);
  ui->progressBar->setMaximum(length);
  ui->globalVolumeSlider->setValue(globalVolume);
  ui->groupVolumeSlider->setValue(groupVolume);
  ui->trackVolumeSlider->setValue(trackVolume);
  ui->groupWeight->setValue(groupWeight);
  ui->trackWeight->setValue(trackWeight);
}

void PlayerWindow::progressChanged(qint64 position) {
  ui->progressBar->setSliderPosition(position);
  ui->time->setText(QString::number(position / 1000) + "/" +
                    QString::number(ui->progressBar->maximum() / 1000));
}

void PlayerWindow::manageHotkey(Player::Hotkey hotkey) {
  switch (hotkey) {
  case Player::Hotkey::previous:
    ui->previous->click();
    break;
  case Player::Hotkey::next:
    ui->next->click();
    break;
  case Player::Hotkey::playback:
    ui->playback->click();
    break;
  case Player::Hotkey::stop:
    ui->stop->click();
    break;
  case Player::Hotkey::settings:
    ui->settings->click();
    break;
  case Player::Hotkey::query:
    // TODO maybe send notification?
    break;
  case Player::Hotkey::trackVolUp:
    ui->trackVolumeSlider->setValue(ui->trackVolumeSlider->value() + 1);
    break;
  case Player::Hotkey::groupVolUp:
    ui->groupVolumeSlider->setValue(ui->groupVolumeSlider->value() + 1);
    break;
  case Player::Hotkey::globalVolUp:
    ui->globalVolumeSlider->setValue(ui->globalVolumeSlider->value() + 1);
    break;
  case Player::Hotkey::trackVolDown:
    ui->trackVolumeSlider->setValue(ui->trackVolumeSlider->value() - 1);
    break;
  case Player::Hotkey::groupVolDown:
    ui->groupVolumeSlider->setValue(ui->groupVolumeSlider->value() - 1);
    break;
  case Player::Hotkey::globalVolDown:
    ui->globalVolumeSlider->setValue(ui->globalVolumeSlider->value() - 1);
    break;
  }
}
