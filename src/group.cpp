﻿#include <QInputDialog>
#include <QJsonArray>
#include <QJsonObject>
#include <QMessageBox>
#include <stdexcept>

#include "group.h"
#include "ui_group.h"

Group::Group(const QJsonObject &groupObject)
    : Group(groupObject["name"].toString(), groupObject["volume"].toInt(50),
            groupObject["weight"].toInt(100)) {
  if (!groupObject["tracks"].isArray())
    throw std::invalid_argument("groupObject is invalid");

  for (QJsonValue trackValue : groupObject["tracks"].toArray()) {
    auto track = std::make_shared<Track>(name, trackValue.toObject());
    addTrack(track);
  }
}

Group::Group(QString name, unsigned volume, unsigned weight)
    : ui(new Ui::Group), name(name), volume(volume), weight(weight),
      tracksWeight(0) {
  if (name == "")
    throw std::invalid_argument("invalid name");
  ui->setupUi(this);

  connect(ui->rename, &QPushButton::clicked, this, &Group::changeName);
  connect(ui->remove, &QPushButton::clicked, this, [this]() {
    auto respone = QMessageBox::question(
        this, "Remove Group",
        "Are you sure you want to remove this group and all of its tracks?");
    if (respone == QMessageBox::Yes)
      emit remove(this->name);
  });
  connect(ui->removeSelected, &QPushButton::clicked, [this]() {
    auto respone = QMessageBox::question(
        this, "Remove Group", "Are you sure you want to remove those tracks?");
    if (respone == QMessageBox::Yes)
      emit removeSelected();
  });

  // TODO update volume and weight if this group is currently played.
  ui->volume->setValue(volume);
  connect(ui->volume, &QAbstractSlider::valueChanged, this, &Group::setVolume);
  ui->weight->setValue(weight);
  connect(ui->weight, QOverload<int>::of(&QSpinBox::valueChanged), this,
          &Group::setWeight);

  connect(ui->filterText, &QLineEdit::textChanged, this, &Group::filterTracks);
}

Group::~Group() {
  for (auto track : tracks)
    track->setParent(nullptr);
}

QString Group::getName() const { return name; }

unsigned Group::getVolume() const { return volume; }

unsigned Group::getWeight() const { return weight; }

double Group::getTotalWeight() const { return tracksWeight * weight; }

void Group::addTrack(std::shared_ptr<Track> track) {
  if (contains(track) == true)
    return; // TODO maybe throw exception?

  ui->tracks->layout()->addWidget(track.get());
  tracks.push_front(track);
  tracksMap[track->getPath().toString()] = tracks.begin();
  tracksWeight += track->getWeight();
  // If the weight of the track changes we need to update weight of all tracks.
  connect(track.get(), &Track::changedWeight, this,
          [this](unsigned oldWeight, unsigned newWeight) {
            tracksWeight -= oldWeight;
            tracksWeight += newWeight;
          });
}

bool Group::contains(std::shared_ptr<Track> track) {
  return tracksMap.find(track->getPath().toString()) != tracksMap.end();
}

void Group::setVolume(unsigned volume) { this->volume = volume; }

void Group::setWeight(unsigned weight) { this->weight = weight; }

std::shared_ptr<Track> Group::getTrack(QString path) const {
  return *tracksMap.at(path);
}

std::shared_ptr<Track> Group::chooseTrack(double weight) const {
  for (auto track : tracks) {
    if (track->getWeight() * this->weight >= weight)
      return track;
    weight -= track->getWeight() * this->weight;
  }
  return std::shared_ptr<Track>();
}

QJsonValue Group::toJson() const {
  QJsonObject groupJson;
  groupJson["name"] = name;
  groupJson["volume"] = static_cast<int>(volume);
  groupJson["weight"] = static_cast<int>(weight);

  QJsonArray tracksJson;
  for (auto track : tracks)
    tracksJson.append(track->toJson());
  groupJson["tracks"] = tracksJson;

  return groupJson;
}

void Group::changeName() {
  QString name =
      QInputDialog::getText(this, tr("Rename Group"), tr("Group name:"));
  QString oldName = this->name;
  this->name = name;
  emit nameChanged(oldName, name);
}

void Group::removeSelected() {
  for (auto i = tracks.begin(); i != tracks.end(); ++i) {
    if ((*i)->isSelected()) {
      (*i)->setParent(nullptr);
      tracksMap.erase((*i)->getPath().toString());
      i = tracks.erase(i);
    }
  }
}

void Group::filterTracks(QString filter) {
  QRegularExpression regex(filter, QRegularExpression::CaseInsensitiveOption);

  for (auto track : tracks)
    track->setVisible(regex.match(track->getPath().fileName()).hasMatch());
}
