TEMPLATE = app

TARGET = splayer

QT += core gui multimedia

QDEP_DEPENDS += Skycoder42/QHotkey
!load(qdep):error("Failed to load qdep feature")

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += "include"

DESTDIR = "build/bin"
OBJECTS_DIR = "build"
UI_DIR = "build/ui"
MOC_DIR = "build/moc"
RCC_DIR = "build/rcc"
QDEP_GENERATED_DIR = "build/qdep"

SOURCES += \
    src/group.cpp \
    src/main.cpp \
    src/player.cpp \
    src/playerwindow.cpp \
    src/settingswindow.cpp \
    src/track.cpp

HEADERS += \
    include/group.h \
    include/player.h \
    include/playerwindow.h \
    include/settingswindow.h \
    include/track.h

FORMS += \
    ui/group.ui \
    ui/playerwindow.ui \
    ui/settingswindow.ui \
    ui/track.ui

RESOURCES += \
    resources/resources.qrc
